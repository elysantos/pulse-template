# Pulse Template
Template padrão para uso em aplicações Vue contendo app bar e controles de login

## Instalação
Usando NPM:
```
npm install pulse-template
```

Usando YARN:
```
yarn add pulse-template
```

## Como usar
```
import App from 'pulse-template'

new Vue({
    router,
    store,
    vuetify,
    render: h => h(App, { props: { keycloak: keycloak } })
}).$mount('#app');


```
